import React, { useEffect, useState } from "react";
import FeatureCart from '../FeatureCart'

const Categories = () => {
  const [categories, setCategories] = useState([]);
  useEffect(() => {
    const fetchCategories = async () => {
      const response = await fetch(
        "https://fakestoreapi.com/products/categories"
      );
      const data = await response.json();
      console.log(data);
      setCategories(data);
    };
    fetchCategories();
  }, []);

  return (
    <div>
      <FeatureCart cards={categories} />
    </div>
  );
};

export default Categories;
